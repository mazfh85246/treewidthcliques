//
// Created by Akram Zaher Farid, Matthew on 07.10.22.
//

#include "grid.h"

const unsigned int multiplier = 1664525;
const unsigned int increment = 1013904223;
int unsigned init_seed;

unsigned int next_random()
{
    init_seed = (multiplier * init_seed + increment);
    return init_seed;
}
