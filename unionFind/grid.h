//
// Created by Akram Zaher Farid, Matthew on 07.10.22.
//

#ifndef TREEWIDTHGRIDS_GRID_H
#define TREEWIDTHGRIDS_GRID_H


#include <vector>
#include <queue>
#include <iostream>
#include <set>

#define LEFT(p) ((p.hor != 0)? (2*n-1)*p.vert + p.hor-1 : -1)
#define RIGHT(p) ((p.hor != n-1) ? (2*n-1)*p.vert + p.hor : -1)
#define UP(p) ((p.vert != 0) ? (2*n-1)*(p.vert) - n + p.hor: -1)
#define DOWN(p) ((p.vert != n-1) ? (2*n-1)*(p.vert) + n-1 + p.hor : -1)

unsigned int next_random();

class grid {
    struct PlanePoint {
        int vert;
        int hor;
    };

    int n = 0;
    std::vector<std::set<int>> cliqueElem;// list of the elements of each clique
    std::vector<std::set<int>> vertexCliques;
    std::vector<bool> eliminated;
    std::vector<int> rand_order;
    int step_number = 0;

public:
    grid(int n) {
        this->n = n;
        int numEdges = 2 * n * n - 2 * n;
        int numVert = n * n;

        for (int i = 0; i < numEdges; i++) {
            cliqueElem.emplace_back();
        }

        vertexCliques = std::vector(numVert, std::set<int>());
        eliminated = std::vector(numVert, false);

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                PlanePoint p = {i, j};
                int index = pToInt(p);
                int left = LEFT(p);
                int right = RIGHT(p);
                int up = UP(p);
                int down = DOWN(p);

                if (left != -1) {
                    cliqueElem[left].insert(index);
                    vertexCliques[index].insert(left);}

                if (right != -1) {
                    cliqueElem[right].insert(index);
                    vertexCliques[index].insert(right);
                }

                if (up != -1) {
                    cliqueElem[up].insert(index);
                    vertexCliques[index].insert(up);
                }

                if (down != -1) {
                    cliqueElem[down].insert(index);
                    vertexCliques[index].insert(down);
                }

                rand_order.push_back(i*n+j);
            }
        }
        genRand(numVert);

    }

    void genRand(int numVert) {// create a random permutation of the vertices of the grid
        for (int i = 0; i < numVert; ++i) {
            int r = (next_random() % (numVert-i)) + i;
            int temp = rand_order[i];
            rand_order[i] = rand_order[r];
            rand_order[r] = temp;
        }
    }

    int play() {
        int result = 0;
        const int numVert = n * n;
        while (result < numVert - step_number ) {
            if (isEliminated(rand_order[step_number])) {
                step_number++;
                continue;
            }
            int deg = eliminate(rand_order[step_number++]);
            result = std::max(result, deg);
        }
        return result;
    }

    grid(grid &g) : cliqueElem(g.cliqueElem), eliminated(g.eliminated), n(g.n){
        vertexCliques.reserve(g.vertexCliques.size());
        rand_order.reserve(eliminated.size());
        for (int i = 0; i < g.vertexCliques.size(); ++i) {
            if (eliminated[i]){
                vertexCliques.emplace_back();
            } else {
                vertexCliques.emplace_back(std::set<int>(g.vertexCliques[i]));
            }
            rand_order.push_back(i);
        }
        // todo: use more time efficient rng
        genRand(vertexCliques.size());
    }

    bool isEliminated(int index){
        return eliminated[index];
    }

    int eliminate(int v) {
        assert(v != -1 && eliminated[v] == false);

        // chose the first clique that v is a part of. We will merge all cliques v participates in into this one
        int resultClique = *vertexCliques[v].begin();
        for (auto clique: vertexCliques[v]) {
            // delete v from all of the cliques
            auto check = cliqueElem[clique].erase(v);
            assert(check == 1);

            if (clique == resultClique) continue;

            // for the rest of the cliques, delete them from their vertices set of cliques, and insert the result clique
            for (int cliqueElement: cliqueElem[clique]) {
                check = vertexCliques[cliqueElement].erase(clique);
                assert(check == 1);

                vertexCliques[cliqueElement].insert(resultClique);
                cliqueElem[resultClique].insert(cliqueElement);
            }
        }

        eliminated[v] = true;
        return cliqueElem[resultClique].size();
    }

private:

    int pToInt(PlanePoint p) {
        if (p.hor < n && p.vert < n && p.hor >= 0 && p.vert >= 0) {
            return n * p.vert + p.hor;
        }
        return -1;
    }

    PlanePoint intToPoint(int v) {
        assert(v < n * n && v >= 0);
        return {v / n, v % n};
    }
};


#endif //TREEWIDTHGRIDS_GRID_H
