#include <iostream>
#include "unionFind/grid.h"

int main() {
    srand(time(nullptr));
    const int n = 10;
    const int numVert = n*n;
    const int numRepeats = 800;
    grid g(n);

    auto start = std::chrono::steady_clock::now();

    int result = 0;
    int stepNumber = 0;
    while (result < numVert-stepNumber){

        int min = numVert*numRepeats;
        int minIndex = 0;


        for (int i = 0; i < numVert; ++i) {
            if (g.isEliminated(i)) continue;

            //int singleBest = 0;
            int singleBest = numVert;
            auto g_i = grid(g);
            g_i.eliminate(i);
            // get the best value for a single vertex out of numRepeats runs
            for (int j = 0; j < numRepeats; ++j) {
                //singleBest += grid(g).play();
                int temp = grid(g_i).play();
                if (temp < singleBest) {
                    singleBest = temp;
                }
            }

            // find the min out of all vertices
            if (singleBest < min){
                min = singleBest;
                minIndex = i;
            }
        }

        int degree = g.eliminate(minIndex);

        if (degree > result) {
            result = degree;
        }
        stepNumber++;
        std::cout << "stepnumber:  " <<  stepNumber << std::endl;
    }

    std::cout << result << std::endl;
    auto end = std::chrono::steady_clock::now();
    std::cout << "Time taken = "
           << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1000000.0 << "[s]"
           << std::endl;
    return 0;
}
