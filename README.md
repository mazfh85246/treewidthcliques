Test project implementing the elimination game on graphs, using their clique graph representation.
The long term goal of this project, is to efficiently implement a new efficient heuristic for calculating treewidth.
